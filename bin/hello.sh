#!/bin/bash

service mysql start

if mysqlshow "${MYSQL_DATABASE}" > /dev/null 2>&1; then
	echo 'EXIST'
else
	mysql -e "CREATE DATABASE ${MYSQL_DATABASE} /*\!40100 DEFAULT CHARACTER SET utf8 */;"
	#mysql ${MYSQL_DATABASE} < /app/bin/test_table.sql
	echo 'DONT'
fi

mysql ${MYSQL_DATABASE} < /app/tdb.sql
mysql -e "CREATE USER IF NOT EXISTS ${MYSQL_USER}@localhost IDENTIFIED BY '${MYSQL_PASSWORD}';"
mysql -e "GRANT ALL PRIVILEGES ON ${MYSQL_DATABASE}.* TO '${MYSQL_USER}'@'localhost';"
mysql -e "FLUSH PRIVILEGES;"

mysql ${MYSQL_DATABASE} < /app/bin/triggers.sql &
wait

/app/bin/update.sh &
/app/bin/log.sh
