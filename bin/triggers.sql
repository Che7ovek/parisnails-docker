CREATE TABLE IF NOT EXISTS app_log(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
	changed_id INT,
	old_name VARCHAR(50),
	new_name VARCHAR(50),
	edit TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

DELIMITER $$

	CREATE TRIGGER IF NOT EXISTS log_update
	AFTER UPDATE
	ON test_table FOR EACH ROW
	BEGIN

		INSERT INTO app_log(changed_id, old_name, new_name)
			VALUES(old.id, old.name, new.name);

	END$$

DELIMITER ;