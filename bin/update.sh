#!/bin/bash

#Немного рандомных имён для замены
names=(Aboba Camilla Pryce Honorata Lal Nicomachus Olle Agneza Pia Marusya Henrik Pericles Vilma Easter Raginmund Joosep Jasmin Braam Hiltrude Joachim Amir Sharise Mar Rothaid Martin Pedro)
while true;
do
	# ${#names[*]} - количество имён
	# $[($RANDOM % @)]] - случайное число от 0 до количества имён
	# ${names[@]} - собственно случайное имя
	rndName=${names[$[($RANDOM % ${#names[*]})]]}
	# Случайный ID из тестовой таблицы
	sqlRandId="SELECT id FROM test_table ORDER BY RAND() LIMIT 1;"
	rndId=`mysql -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -N -e "$sqlRandId"`
	# Обновляем запись
	sqlUpdate="UPDATE test_table SET name = '${rndName}' WHERE id = ${rndId};"
	mysql -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -N -e "$sqlUpdate"
	#Дамп базы после изменения
	mysqldump ${MYSQL_DATABASE} > /app/tdb.sql
	sleep ${SLEEP_TIME}
done