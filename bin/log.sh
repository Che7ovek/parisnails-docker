#!/bin/bash

function getMaxSize {
	out=( $(grep -Eo '[[:digit:]]+|[^[:digit:]]+' <<<"${MAX_FILE_SIZE}") )
	if [ ${out[1]} == 'KB' ]; then
		maxSize=${out[0]}*1024
	elif [ ${out[1]} == 'MB' ]; then
		maxSize=${out[0]}*1024*1024
	else
		maxSize=${out[0]}
	fi
	echo $(( $maxSize ))
}
function timestamp() {
	date +"%Y%m%d%H%M%S"
}

maxSize=$(getMaxSize)

while true;
do
	sqlCount="SELECT COUNT(*) FROM app_log;"
	rowsCount=`mysql -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -N -e "$sqlCount"`
	if [[ $rowsCount -gt 0 ]]; then
		# Выбираем ровно столько, сколько было при проверке
		sqlSelect="SELECT * 
			FROM app_log 
			ORDER BY edit 
			LIMIT $rowsCount;"
		
		mysql --batch -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -N -e "$sqlSelect" | while read -r id changed_id old_name new_name edit;
		do
			echo "$changed_id;$old_name;$new_name;$edit" >> /app/${LOG_FILE_NAME};
			sqlDelete="DELETE FROM app_log WHERE id=$id;"
			mysql -u ${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} -N -e "$sqlDelete"
			logsize=$(wc -c < /app/${LOG_FILE_NAME})
			if [[ $logsize -ge $maxSize ]]; then
				newFileName="${LOG_FILE_NAME}.$(timestamp).gz"
				gzip -c /app/${LOG_FILE_NAME} > /app/logs/${newFileName}
				rm /app/${LOG_FILE_NAME}
			fi
		done 
	fi
	sleep ${SLEEP_TIME}
done